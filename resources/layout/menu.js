function request(url) {
  return new Promise(function (resolve, reject) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onload = resolve;
    xhr.onerror = reject;
    xhr.send();
  });
}
(function() {
  // automatically import menu and prepend it to body
  request('{root}/resources/layout/menu.html').then((e) => {
    let target = document.querySelector('body')
    let div = document.createElement('div')
    div.innerHTML = e.target.responseText
    target.className = "has-navbar-fixed-top"
    target.prepend(div)
  })
})();