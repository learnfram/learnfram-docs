## Microservice JSON Schema

### Microservices JSON Schema specification
- https://learnfram.gitlab.io/learnfram-docs/schema/microservices/schema.json

### Check you Javascript Schema 
- https://learnfram.gitlab.io/learnfram-docs/schema/

### Microservices examples
- [DataOrganized_RandomCrop.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/DataOrganized_RandomCrop.json)
- [DimensionReductor_CompSelect.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/DimensionReductor_CompSelect.json)
- [DimensionReductor_PCA.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/DimensionReductor_PCA.json)
- [DimensionReductor_PCA_auto.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/DimensionReductor_PCA_auto.json)
- [GaussianDenoiser.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/GaussianDenoiser.json)
- [L1RegularizedDenoiser.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/L1RegularizedDenoiser.json)
- [L2RegularizedDenoiser.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/L2RegularizedDenoiser.json)
- [LpRegularizedDenoiser.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/LpRegularizedDenoiser.json)
- [MeanDenoiser.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/MeanDenoiser.json)
- [ModelDesigner_CNN.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/ModelDesigner_CNN.json)
- [ModelDesigner_OneClassSVM.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/ModelDesigner_OneClassSVM.json)
- [ModelDesigner_OneClassSVM_auto.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/ModelDesigner_OneClassSVM_auto.json)
- [TemporalFeatureExtractor.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/TemporalFeatureExtractor.json)
- [WavesFeatureExtractor.json](https://learnfram.gitlab.io/learnfram-docs/schema/microservices/examples/WavesFeatureExtractor.json)